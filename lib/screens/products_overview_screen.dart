import 'package:flutter/material.dart';

import '../models/product.dart';
import '../widget/product_item.dart';

class ProductsOverviewScreen extends StatelessWidget {
  final List<Product> loadedProducts = [
    Product(
      id: 'p1',
      title: 'The North Face',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl: 'https://intertop.ua/load/N21306/1600x2133/MAIN.jpg',
    ),
    Product(
      id: 'p2',
      title: 'Кросівки унісекс EA7',
      description: 'A nice pair of trousers.',
      price: 199.99,
      imageUrl: 'https://intertop.ua/load/7S27/1600x2133/MAIN.jpg',
    ),
    Product(
      id: 'p3',
      title: 'Кросівки чоловічі Skechers',
      description: 'Warm and cozy - exactly what you need for the winter.',
      price: 19.99,
      imageUrl: 'https://intertop.ua/load/KM3509/1600x2133/MAIN.jpg',
    ),
    Product(
      id: 'p4',
      title: 'Рюкзак Tommy Hilfiger',
      description: 'Prepare any meal you want.',
      price: 79.99,
      imageUrl: 'https://intertop.ua/load/TC1402/1600x2133/MAIN.jpg',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text('Excellent Shop'),
      ),
      body: GridView.builder(
        padding: const EdgeInsets.all(10.0),
        itemCount: loadedProducts.length,
        itemBuilder: (ctx, index) => ProductItem(
          id: loadedProducts[index].id,
          title: loadedProducts[index].title,
          imageUrl: loadedProducts[index].imageUrl,
        ),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
        ),
      ),
    );
  }
}
